use serde::Serialize;

#[derive(Debug, Queryable, Serialize)]
pub struct Ingredient {
    pub food_id: i32,
    pub recipe_id: i32,
    pub weight_g: i32,
}
