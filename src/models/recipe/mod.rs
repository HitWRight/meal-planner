pub mod ingredient;

use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize, Queryable)]
pub struct Recipe {
    pub id: i32,
    pub user_id: i32,
    pub name: String,
}

impl Recipe {
    pub fn new(user_id: i32, name: String) -> Recipe {
        Recipe {
            id: 0,
            user_id,
            name,
        }
    }
}
