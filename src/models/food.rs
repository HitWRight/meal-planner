use rocket::serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, FromForm, Serialize, Queryable)]
pub struct Food {
    pub id: i32,
    pub carbohydrates: f32,
    pub fat: f32,
    pub kcal: f32,
    pub name: String,
    pub protein: f32,
}

impl Food {
    pub fn new(name: String, kcal: f32, protein: f32, carbohydrates: f32, fat: f32) -> Food {
        Food {
            id: 0,
            name,
            carbohydrates,
            fat,
            kcal,
            protein,
        }
    }
}
