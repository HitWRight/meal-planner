use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, Queryable)]
pub struct User {
    pub id: i32,
    pub username: String,
}
