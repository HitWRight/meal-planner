use crate::models::food::Food;
use crate::models::recipe::ingredient::Ingredient;
use crate::models::recipe::Recipe;
use crate::Db;
use diesel::prelude::*;
use rocket::form::Form;
use rocket::response::Redirect;
use rocket_dyn_templates::Template;
use serde::Serialize;

#[derive(Serialize)]
struct RecipeData {
    items: Vec<Recipe>,
}

pub fn redirect_to_login() -> Redirect {
    Redirect::to(uri!("/login/", crate::views::login::panel))
}

#[get("/")]
pub async fn list(db: Db) -> Template {
    use crate::schema::recipes::dsl::{name, recipes};
    let context = RecipeData {
        items: db
            .run(|db| {
                recipes
                    .order(name)
                    .load::<Recipe>(db)
                    .expect("Faile to load Recipe")
            })
            .await,
    };

    Template::render("recipe/index", context)
}

#[get("/", rank = 2)]
pub fn redirect_to_login_from_index() -> Redirect {
    redirect_to_login()
}

#[derive(Debug, Serialize)]
struct RecipeContext {
    pub recipe: Recipe,
    pub ingredients: Vec<Ingredient>,
}

#[get("/<recipe_name>")]
pub async fn item(recipe_name: String, db: Db) -> Template {
    use crate::schema::recipes::dsl::{name, recipes, user_id};

    let recipe_name_clone = recipe_name.clone();
    let recipe = db
        .run(move |db| {
            recipes
                .filter(user_id.eq(1))
                .filter(name.eq(&recipe_name_clone))
                .load::<Recipe>(&*db)
                .expect("Failed to load recipes")
        })
        .await;

    debug_assert!(
        recipe.len() <= 1,
        "names in recipes should not contain duplicates"
    );

    let recipe = recipe
        .into_iter()
        .nth(0)
        .unwrap_or(Recipe::new(1, recipe_name));

    use crate::schema::ingredients::dsl::*;
    let recipe_id_clone = recipe.id;
    let other_ingredients = db
        .run(move |db| {
            ingredients
                .filter(recipe_id.eq(recipe_id_clone))
                .load::<Ingredient>(db)
                .expect("Failed to load ingredients")
        })
        .await;

    let context = RecipeContext {
        recipe: recipe,
        ingredients: other_ingredients,
    };

    Template::render("recipe/item", context)
}

#[get("/<_recipe_name>", rank = 2)]
pub fn redirect_to_login_from_item(_recipe_name: String) -> Redirect {
    redirect_to_login()
}

#[derive(Debug, FromForm)]
pub struct PostForm {
    id: i32,
    user_id: i32,
    ingredient0: Option<String>,
    weight0: Option<i32>,
    ingredient1: Option<String>,
    weight1: Option<i32>,
    ingredient2: Option<String>,
    weight2: Option<i32>,
    ingredient3: Option<String>,
    weight3: Option<i32>,
    ingredient4: Option<String>,
    weight4: Option<i32>,
    ingredient5: Option<String>,
    weight5: Option<i32>,
    ingredient6: Option<String>,
    weight6: Option<i32>,
    ingredient7: Option<String>,
    weight7: Option<i32>,
    ingredient8: Option<String>,
    weight8: Option<i32>,
    ingredient9: Option<String>,
    weight9: Option<i32>,
    ingredient10: Option<String>,
    weight10: Option<i32>,
    ingredient11: Option<String>,
    weight11: Option<i32>,
    ingredient12: Option<String>,
    weight12: Option<i32>,
    ingredient13: Option<String>,
    weight13: Option<i32>,
    ingredient14: Option<String>,
    weight14: Option<i32>,
    ingredient15: Option<String>,
    weight15: Option<i32>,
    ingredient16: Option<String>,
    weight16: Option<i32>,
    ingredient17: Option<String>,
    weight17: Option<i32>,
    ingredient18: Option<String>,
    weight18: Option<i32>,
    ingredient19: Option<String>,
    weight19: Option<i32>,
}

#[post("/<recipe_name>", data = "<recipe_form>")]
pub fn edit(recipe_name: String, recipe_form: Form<PostForm>, db: Db) -> Redirect {
    todo!()
    // use crate::schema::recipes::dsl::{name, recipes, user_id};
    // println!("{:#?}", recipe_form);

    // let recipe_list = recipes
    //     .filter(user_id.eq(user.id))
    //     .filter(name.eq(&recipe_name))
    //     .load::<Recipe>(&*db)
    //     .expect("Failed to load recipes");

    // match recipe_list.len() {
    //     0 => {
    //         let result = insert_into(recipes)
    //             .values((user_id.eq(recipe_form.user_id), name.eq(&recipe_name)))
    //             .execute(&*db)
    //             .expect("Failed to insert");
    //         println!("{:#?}", result);
    //     }
    //     1 => {
    //         let mut changeset = recipe_list[0].clone();
    //         changeset.name = recipe_name.clone();
    //         changeset.user_id = user.id;
    //         let result = diesel::update(
    //             recipes
    //                 .filter(user_id.eq(user.id))
    //                 .filter(name.eq(&recipe_name)),
    //         )
    //         .set(changeset)
    //         .execute(&*db)
    //         .expect("failed to update");
    //         println!("{:?}", result);
    //     }
    //     _ => {
    //         panic!("There shouldn't be 2 recipes ")
    //     }
    // };

    // let updated_recipe_id = recipes
    //     .filter(user_id.eq(user.id))
    //     .filter(name.eq(&recipe_name))
    //     .load::<Recipe>(&*db)
    //     .expect("Failed to load recipes")
    //     .into_iter()
    //     .nth(0)
    //     .expect("Recipe should be added before this executed already.")
    //     .id;

    // //delete all ingredients
    // use crate::schema::ingredients::dsl::{food_id, ingredients, recipe_id, weight_g};
    // let result = diesel::delete(ingredients)
    //     .filter(recipe_id.eq(updated_recipe_id))
    //     .execute(&*db);

    // println!("{:?}", result);
    // // info!("Deleted ingredients for recipe: {}", result);

    // //save all ingredients
    // let ingredients_list = construct_ingredients(updated_recipe_id, recipe_form, &db);
    // println!("constructed: {:?}", ingredients_list);
    // for ingredient in ingredients_list {
    //     insert_into(ingredients)
    //         .values((
    //             food_id.eq(ingredient.food_id),
    //             recipe_id.eq(ingredient.recipe_id),
    //             weight_g.eq(ingredient.weight_g),
    //         ))
    //         .execute(&*db)
    //         .unwrap();
    // }
    // Redirect::to(uri!("/recipe/", item(recipe_name)))
}

// TODO: Rocket 0.4 doesn't provide guards for arrays, implement own
fn construct_ingredients(recipe_id: i32, recipe_form: Form<PostForm>, db: &Db) -> Vec<Ingredient> {
    let mut ingredients_list = vec![];
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient0.clone(), db),
        recipe_form.weight0,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient1.clone(), db),
        recipe_form.weight1,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient2.clone(), db),
        recipe_form.weight2,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient3.clone(), db),
        recipe_form.weight3,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient4.clone(), db),
        recipe_form.weight4,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient5.clone(), db),
        recipe_form.weight5,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient6.clone(), db),
        recipe_form.weight6,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient7.clone(), db),
        recipe_form.weight7,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient8.clone(), db),
        recipe_form.weight8,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient9.clone(), db),
        recipe_form.weight9,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient10.clone(), db),
        recipe_form.weight10,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient11.clone(), db),
        recipe_form.weight11,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient12.clone(), db),
        recipe_form.weight12,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient13.clone(), db),
        recipe_form.weight13,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient14.clone(), db),
        recipe_form.weight14,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient15.clone(), db),
        recipe_form.weight15,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient16.clone(), db),
        recipe_form.weight16,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient17.clone(), db),
        recipe_form.weight17,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient18.clone(), db),
        recipe_form.weight18,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };
    if let (Some(food), Some(weight_g)) = (
        load_food_by_name(recipe_form.ingredient19.clone(), db),
        recipe_form.weight19,
    ) {
        ingredients_list.push(Ingredient {
            food_id: food.id,
            recipe_id,
            weight_g,
        })
    };

    ingredients_list
}

fn load_food_by_name(food_name: Option<String>, _db: &Db) -> Option<Food> {
    if food_name == None {
        return None;
    }
    todo!()
}
