use crate::models::food::Food;
use crate::Db;
use rocket_dyn_templates::Template;
use serde::Serialize;

#[derive(Serialize)]
struct FoodData {
    name: String,
    items: Vec<Food>,
}

#[get("/<date>")]
pub fn list(date: String, db: Db) -> Template {
    Template::render("usage/index", ())
}
