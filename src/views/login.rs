use crate::models::user::User;
use crate::Db;
use diesel::prelude::*;
use rocket::form::Form;
use rocket::http::Cookie;
use rocket::http::CookieJar;
use rocket::outcome::IntoOutcome;
use rocket::request::FromRequest;
use rocket::request::Outcome;
use rocket::request::Request;
use rocket::response::Redirect;
use rocket_dyn_templates::Template;
use serde::Serialize;

#[derive(Serialize)]
struct UserData {}

#[derive(FromForm)]
pub struct LoginData {
    pub username: String,
}

#[get("/")]
pub fn authenticated_panel() -> &'static str {
    "Welcome (Add logout button here)"
}

#[get("/", rank = 2)]
pub fn panel() -> Template {
    let user_data = UserData {};
    Template::render("login/login", user_data)
}

#[post("/", data = "<login_form>")]
pub async fn login(login_form: Form<LoginData>, db: Db, cookies: &CookieJar<'_>) -> Redirect {
    use crate::schema::users::dsl::*;

    let user = db
        .run(move |db| {
            users
                .filter(username.eq(&login_form.username))
                .load::<User>(&*db)
                .unwrap()
        })
        .await;

    assert_eq!(
        user.len(),
        1,
        "Usernames should be unique or no user found!"
    );
    cookies.add_private(Cookie::new("user_id", user[0].id.to_string()));
    Redirect::to(uri!("/login/", authenticated_panel))
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for User {
    type Error = ();

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        req.cookies()
            .get_private("user_id")
            .and_then(|cookie| cookie.value().parse().ok())
            .map(|id| User {
                id,
                username: "".to_string(),
            })
            .or_forward(())
    }
}
