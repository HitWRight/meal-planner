use crate::models::food::Food;
use crate::Db;
use diesel::insert_into;
use diesel::prelude::*;
use rocket::form::Form;
use rocket::response::Redirect;
use rocket::serde::json::Json;
use rocket_dyn_templates::Template;
use serde::Serialize;

#[derive(Serialize)]
struct FoodData {
    name: String,
    items: Vec<Food>,
}

#[get("/?json")]
pub async fn list_json(db: Db) -> Json<Vec<Food>> {
    println!("JEbat");
    use crate::schema::foods::dsl::*;
    Json(
        db.run(|db| foods.order(name).load::<Food>(&*db).unwrap())
            .await,
    )
}

#[get("/")]
pub async fn list(db: Db) -> Template {
    use crate::schema::foods::dsl::*;
    let context = FoodData {
        name: "Current food list".to_string(),
        items: db.run(|c| foods.order(name).load::<Food>(c).unwrap()).await,
    };
    Template::render("food/index", context)
}

#[get("/<food_name>")]
pub async fn item(food_name: String, db: Db) -> Template {
    use crate::schema::foods::dsl::*;

    let food_name_clone = food_name.clone();
    let context = db
        .run(move |db| {
            foods
                .filter(name.eq(&food_name_clone))
                .load::<Food>(db)
                .expect("failed to load foods")
        })
        .await;
    debug_assert!(
        context.len() <= 1,
        "names if food table should not contain duplicates"
    );

    Template::render(
        "food/item",
        context
            .first()
            .unwrap_or(&Food::new(food_name, 0.0, 0.0, 0.0, 0.0)),
    )
}

#[post("/<food_name>", data = "<food_form>")]
pub async fn edit(food_name: String, food_form: Form<Food>, db: Db) -> Redirect {
    use crate::schema::foods::dsl::*;

    let food_name_clone = food_name.clone();
    let food = db
        .run(move |db| {
            foods
                .filter(name.eq(&food_name_clone))
                .load::<Food>(db)
                .unwrap()
        })
        .await;

    debug_assert!(
        food.len() <= 1,
        "names if food table should not contain duplicates"
    );

    if food.len() == 1 {
        println!("updating food");
        let food_name_clone = food_name.clone();
        db.run(move |db| {
            diesel::update(foods.filter(name.eq(&food_name_clone)))
                .set((
                    carbohydrates.eq(food_form.carbohydrates),
                    fat.eq(food_form.fat),
                    kcal.eq(food_form.kcal),
                    protein.eq(food_form.protein),
                ))
                .execute(db)
                .unwrap()
        })
        .await;
    } else {
        println!("Adding new food");
        db.run(move |db| {
            insert_into(foods)
                .values((
                    carbohydrates.eq(food_form.carbohydrates),
                    fat.eq(food_form.fat),
                    kcal.eq(food_form.kcal),
                    name.eq(food_form.name.clone()),
                    protein.eq(food_form.protein),
                ))
                .execute(&*db)
                .unwrap()
        })
        .await;
    }

    Redirect::to(uri!("/food/", item(food_name)))
}
