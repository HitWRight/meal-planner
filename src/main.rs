//! An Application for meal tracking and planning
//!
//! Created to learn more about Rocket framework and Rust ecosystem
//!

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate rocket;

mod config;
mod models;
mod schema;
mod views;

use rocket::fs::FileServer;
use rocket_dyn_templates::Template;
use rocket_sync_db_pools::database;
use serde::Serialize;

#[database("nutrient_tracker")]
pub struct Db(pub diesel::PgConnection);

#[get("/test")]
fn hello() -> &'static str {
    "Hello, World!"
}

#[derive(Serialize)]
struct IndexContext {}

#[get("/")]
fn index() -> Template {
    let context = IndexContext {};

    Template::render("index", context)
}

#[launch]
fn rocket() -> _ {
    dotenv::dotenv().ok();
    rocket::build()
        .mount("/", routes![index, hello])
        .mount(
            "/login/",
            routes![
                views::login::authenticated_panel,
                views::login::panel,
                views::login::login
            ],
        )
        .mount(
            "/food/",
            routes![
                views::food::list_json,
                views::food::list,
                views::food::item,
                views::food::edit
            ],
        )
        .mount(
            "/recipe/",
            routes![
                views::recipe::list,
                views::recipe::item,
                views::recipe::edit,
                views::recipe::redirect_to_login_from_item,
                views::recipe::redirect_to_login_from_index
            ],
        )
        .mount("/static", FileServer::from("./static"))
        .attach(Db::fairing())
        .attach(Template::fairing())
}
