use crate::models::food::Food;
use crate::models::user::User;
use std::io::prelude::*;

pub struct Config {
    pub users: Vec<User>,
    pub foods: Vec<Food>,
}

impl Config {
    pub fn load() -> Self {
        Config {
            users: serde_json::from_str(
                &std::fs::read_to_string("assets/users.json").unwrap_or("[]".to_string()),
            )
            .unwrap(),
            foods: serde_json::from_str(
                &std::fs::read_to_string("assets/foods.json").unwrap_or("[]".to_string()),
            )
            .unwrap(),
        }
    }

    pub fn save(self) {
        std::fs::File::create("assets/users.json")
            .unwrap()
            .write_all(
                serde_json::to_string_pretty(&self.users)
                    .unwrap()
                    .as_bytes(),
            )
            .unwrap();
        std::fs::File::create("assets/foods.json")
            .unwrap()
            .write_all(
                serde_json::to_string_pretty(&self.foods)
                    .unwrap()
                    .as_bytes(),
            )
            .unwrap();
    }
}
