use rocket::request::FromRequest;
use rocket::Outcome;
use rocket::Request;

pub struct UserToken(String);

#[derive(Debug)]
enum UserTokenError {
    BadCount,
    Missing,
    Invalid,
    Unauthorized,
}

impl<'a, 'r> FromRequest<'a, 'r> for UserToken {
    type Error = UserTokenError;

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let keys: Vec<_> = request.headers().get("Authentication").collect();
        match keys.len() {
            0 => Outcome::Failure((rocket::http::Status::BadRequest, UserTokenError::Missing)),
            1 => match get_user(keys[0]) {
                -1 => Outcome::Failure((
                    rocket::http::Status::Unauthorized,
                    UserTokenError::Unauthorized,
                )),
                0 => panic!("Should not be possible to have a user with ID 0"),
                _ => Outcome::Success(UserToken("Admin".to_string())),
            },
            _ => Outcome::Failure((rocket::http::Status::BadRequest, UserTokenError::BadCount)),
        }
    }
}

fn get_user(token: &str) -> i32 {
    //TODO: implement check for login
    1
}
