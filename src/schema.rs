table! {
    foods (id) {
        id -> Int4,
        carbohydrates -> Float4,
        fat -> Float4,
        kcal -> Float4,
        name -> Text,
        protein -> Float4,
    }
}

table! {
    ingredients (food_id, recipe_id) {
        food_id -> Int4,
        recipe_id -> Int4,
        weight_g -> Int4,
    }
}

table! {
    recipes (id) {
        id -> Int4,
        user_id -> Int4,
        name -> Text,
    }
}

table! {
    usages (date, user_id) {
        date -> Date,
        user_id -> Int4,
        food_id -> Nullable<Int4>,
        recipe_id -> Nullable<Int4>,
        usage_g -> Float4,
    }
}

table! {
    users (id) {
        id -> Int4,
        username -> Text,
    }
}

joinable!(ingredients -> foods (food_id));
joinable!(ingredients -> recipes (recipe_id));
joinable!(recipes -> users (user_id));
joinable!(usages -> foods (food_id));
joinable!(usages -> recipes (recipe_id));
joinable!(usages -> users (user_id));

allow_tables_to_appear_in_same_query!(foods, ingredients, recipes, usages, users,);
