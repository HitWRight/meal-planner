-- Your SQL goes here
create table usages(
  date date not null,
  user_id integer references users(id) not null,
  food_id integer references foods(id) null,
  recipe_id integer references recipes(id) null,
  usage_g real not null,
  primary key (date, user_id),
  constraint chk_food_fk check
             (food_id is not null or recipe_id is not null)
);

--drop table usages;
