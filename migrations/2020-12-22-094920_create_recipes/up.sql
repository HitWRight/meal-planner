-- Your SQL goes here
create table recipes (
  id serial primary key not null,
  user_id integer references users(id) not null,
  name text not null
);

--migrate foods
create table foods_new (
  id serial primary key not null,
  carbohydrates real not null,
  fat real not null,
  kcal real not null,
  name text not null,
  protein real not null
);

insert into foods_new(name, carbohydrates, fat, kcal, protein)
select name, carbohydrates, fat, kcal, protein from foods;

drop table foods;
alter table foods_new rename to foods;

create table ingredients (
  food_id integer references foods(id) not null,
  recipe_id integer references recipes(id) not null,
  weight_g real not null,
  primary key (food_id, recipe_id)
);
