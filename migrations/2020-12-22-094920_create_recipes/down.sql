-- This file should undo anything in `up.sql`
drop table ingredients;

--migrate back foods
create table foods_new(
  name varchar(255) primary key not null,
  carbohydrates real not null,
  fat real not null,
  kcal real not null,
  protein real not null
);

insert into foods_new(name, carbohydrates, fat, kcal, protein)
select name, carbohydrates, fat, kcal, protein from foods;

drop table foods;
alter table foods_new rename to foods;

drop table recipes;
