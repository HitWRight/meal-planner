-- Your SQL goes here
create table foods (
  name varchar(255) primary key not null,
  carbohydrates real not null,
  fat real not null,
  kcal real not null,
  protein real not null
);

insert into foods
values ('Pomidorai', 4, 0, 18, 1);
