function getJson(url, callback) {
  console.log("url: " + url);
  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);
  xhr.responseType = "json";
  xhr.onload = function () {
    if (xhr.status === 200) {
      console.log("Loaded");
      callback(null, xhr.response);
    }
  };
  xhr.send();
}

let ingredients = [];
getJson("/food?json", function (err, data) {
  if (err !== null) {
    alert("Script failed");
  } else {
    ingredients = data;
    console.log(ingredients);
    let ingredients_names = data.map(function (elem) {
      return elem.name;
    });

    autocomplete(document.getElementById("ingredient0"), ingredients_names);
    autocomplete(document.getElementById("ingredient1"), ingredients_names);
    autocomplete(document.getElementById("ingredient2"), ingredients_names);
    autocomplete(document.getElementById("ingredient3"), ingredients_names);
    autocomplete(document.getElementById("ingredient4"), ingredients_names);
    autocomplete(document.getElementById("ingredient5"), ingredients_names);
    autocomplete(document.getElementById("ingredient6"), ingredients_names);
    autocomplete(document.getElementById("ingredient7"), ingredients_names);
    autocomplete(document.getElementById("ingredient8"), ingredients_names);
    autocomplete(document.getElementById("ingredient9"), ingredients_names);
    autocomplete(document.getElementById("ingredient10"), ingredients_names);
    autocomplete(document.getElementById("ingredient11"), ingredients_names);
    autocomplete(document.getElementById("ingredient12"), ingredients_names);
    autocomplete(document.getElementById("ingredient13"), ingredients_names);
    autocomplete(document.getElementById("ingredient14"), ingredients_names);
    autocomplete(document.getElementById("ingredient15"), ingredients_names);
    autocomplete(document.getElementById("ingredient16"), ingredients_names);
    autocomplete(document.getElementById("ingredient17"), ingredients_names);
    autocomplete(document.getElementById("ingredient18"), ingredients_names);
    autocomplete(document.getElementById("ingredient19"), ingredients_names);
  }
});

function calculatorTop() {
  let weights = [
    Number(document.getElementsByName("weight0")[0].value),
    Number(document.getElementsByName("weight1")[0].value),
    Number(document.getElementsByName("weight2")[0].value),
    Number(document.getElementsByName("weight3")[0].value),
    Number(document.getElementsByName("weight4")[0].value),
    Number(document.getElementsByName("weight5")[0].value),
    Number(document.getElementsByName("weight6")[0].value),
    Number(document.getElementsByName("weight7")[0].value),
    Number(document.getElementsByName("weight8")[0].value),
    Number(document.getElementsByName("weight9")[0].value),
    Number(document.getElementsByName("weight10")[0].value),
    Number(document.getElementsByName("weight11")[0].value),
    Number(document.getElementsByName("weight12")[0].value),
    Number(document.getElementsByName("weight13")[0].value),
    Number(document.getElementsByName("weight14")[0].value),
    Number(document.getElementsByName("weight15")[0].value),
    Number(document.getElementsByName("weight16")[0].value),
    Number(document.getElementsByName("weight17")[0].value),
    Number(document.getElementsByName("weight18")[0].value),
    Number(document.getElementsByName("weight19")[0].value),
  ];

  let selections = [
    document.getElementsByName("ingredient0")[0].value,
    document.getElementsByName("ingredient1")[0].value,
    document.getElementsByName("ingredient2")[0].value,
    document.getElementsByName("ingredient3")[0].value,
    document.getElementsByName("ingredient4")[0].value,
    document.getElementsByName("ingredient5")[0].value,
    document.getElementsByName("ingredient6")[0].value,
    document.getElementsByName("ingredient7")[0].value,
    document.getElementsByName("ingredient8")[0].value,
    document.getElementsByName("ingredient9")[0].value,
    document.getElementsByName("ingredient10")[0].value,
    document.getElementsByName("ingredient11")[0].value,
    document.getElementsByName("ingredient12")[0].value,
    document.getElementsByName("ingredient13")[0].value,
    document.getElementsByName("ingredient14")[0].value,
    document.getElementsByName("ingredient15")[0].value,
    document.getElementsByName("ingredient16")[0].value,
    document.getElementsByName("ingredient17")[0].value,
    document.getElementsByName("ingredient18")[0].value,
    document.getElementsByName("ingredient19")[0].value,
  ];

  console.log(weights);
  console.log(selections);
  console.log(ingredients);

  let weightSum = weights.reduce(function (total, num) {
    return total + num;
  });

  let i;
  let kcal = 0;
  let protein = 0;
  let carbohydrates = 0;
  let fat = 0;
  for (i = 0; i < selections.length; i++) {
    let found = ingredients.find(function (ing) {
      return ing.name === selections[i];
    });
    if (found) {
      console.log(found);
      kcal += (weights[i] / weightSum) * found.kcal;
      protein += (weights[i] / weightSum) * found.protein;
      carbohydrates += (weights[i] / weightSum) * found.carbohydrates;
      fat += (weights[i] / weightSum) * found.fat;
    }
  }

  let finalWeight = Number(document.getElementById("finalWeight").value);
  console.log(finalWeight);

  if (finalWeight !== 0) {
    kcal = (kcal * weightSum) / finalWeight;
    console.log(kcal);
    protein = (protein * weightSum) / finalWeight;
    fat = (fat * weightSum) / finalWeight;
    carbohydrates = (carbohydrates * weightSum) / finalWeight;
  }

  document.getElementById("kcal").value = kcal;
  document.getElementById("prots").value = protein;
  document.getElementById("fat").value = fat;
  document.getElementById("carbs").value = carbohydrates;
}
