function autocomplete(input, arr) {
  let currentFocus;

  input.addEventListener("input", function (e) {
    var a,
      b,
      i,
      val = this.value;
    closeAllLists();
    currentFocus = -1;

    var list = document.createElement("div");
    list.setAttribute("id", this.id + "autocomplete-list");
    list.setAttribute("class", "autocomplete-items");
    for (i = 0; i < arr.length; i++) {
      let pos = arr[i].toUpperCase().indexOf(val.toUpperCase());
      if (pos != -1) {
        let list_elem = document.createElement("a");
        list_elem.innerHTML = arr[i].substr(0, pos);
        list_elem.innerHTML +=
          "<strong>" + arr[i].substr(pos, val.length) + "</strong>";
        list_elem.innerHTML += arr[i].substr(pos + val.length);
        list_elem.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
        list_elem.addEventListener("click", function (e) {
          input.value = this.getElementsByTagName("input")[0].value;
          closeAllLists();
        });
        list.appendChild(list_elem);
      }
    }

    this.parentNode.appendChild(list);
  });

  input.addEventListener("keydown", function (e) {
    var x = document.getElementById(this.Id + "autocomplete-list");
    if (x) x = x.getElementsByTagName("div");
    if (e.keyCode == 40) {
      currentFocus++;
    } else if (e.keyCode == 38) {
      currentFocus++;
    } else if (e.keyCode == 13) {
      e.preventDefault();
      if (currentFocus > -1) {
        if (x) x[currentFocus].click();
      }
    }
  });

  function addActive(x) {
    if (!x) return false;
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocux < 0) currentFocux = x.length - 1;
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }

  function closeAllLists(element) {
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (element != x[i] && element != input) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
}
